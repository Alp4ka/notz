from django.db import models
from django.contrib.auth.models import AbstractUser
import datetime
import django.utils
import random


class MyUser(AbstractUser):
    def activate(self):
        self.activated = True

    def full_name(self):
        return self.last_name + ' ' + self.first_name+' '+self.midname


class Subject(models.Model):
    name = models.CharField(max_length=128, default='')
    hometask = models.ForeignKey(to='Hometask', null=True, on_delete=models.SET_NULL, related_name='homework')

class Day(models.Model):
    name = models.CharField(max_length=128, default='')
    schedule = models.ManyToManyField(to='Subject', related_name='schedule')
    note = models.CharField(max_length=300, default='')

    def return_schedule(self):
        res = [i for i in self.schedule.all()]
        return res


class Punkt(models.Model):
    text = models.CharField(max_length=300, default='')
    ready = models.BooleanField(default=False)


class Hometask(models.Model):
    punkts = models.ManyToManyField(to='Punkt', related_name="punkts", default='None')
    def check_completed(self):
        res = True
        for i in  self.punks.all():
            if i.ready == False:
                res = False
                break
        return res

    def return_punkts(self):
        return list([i for i in self.punkts.all()])


    def return_str(self):
        res = ''
        print(self.return_punkts())
        for i in self.return_punkts():
            if i.ready:
                res += i.text + ': ' + '<b style="color:#00c92b">Сделано</b>' + '<br>' if i.text != '' else ''
            else:
                res += i.text + ': ' + '<b style="color:#e44c4c">WIP</b>' + '<br>' if i.text != '' else ''
        return res


    def return_str_non(self):
        res = ''
        print(self.return_punkts())
        for i in self.return_punkts():
            if i.ready:
                res += i.text + '>' + '!'
            else:
                res += i.text + '<' + '!'
        return res