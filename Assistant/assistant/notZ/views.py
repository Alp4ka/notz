from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
import django.contrib.auth
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.shortcuts import render
from .models import *
from django.contrib.auth.hashers import *
import hashlib
import datetime
from django.shortcuts import redirect
from django.http import HttpResponse, HttpResponseNotFound
import smtplib


def init_days(request):
    '''l = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']
    for i in l:
        day = Day(name=i)
        day.save()'''
    return render(request, 'index.html', dict())

@login_required(login_url='/login')
def show_schedule(request):
    context=dict()

    days = sorted([i for i in Day.objects.all()], key=lambda x: x.id)
    context['f'] = days[0].note
    context['s'] = days[1].note
    context['t'] = days[2].note
    context['fo'] = days[3].note
    context['fi'] = days[4].note
    context['si'] = days[5].note

    for i in days:
        context[str(i.name)] = [i for i in i.schedule.all()]
    return render(request, 'schedule.html', context)

def registration(request):
    context = dict()
    if request.method == 'POST':
        form = request.POST
        if form['username'] != '' and form['password1'] != '' and form['password2'] != '' and len(form['username']) > 4 and form['password1'] == form['password2']:
            user = MyUser.objects.create_user(username = form['username'], password=form['password1'])
            user.save()
            context['message'] = 'Зареган!'
            return HttpResponseRedirect("/login")
        else:
            context['message'] = 'Проверьте правильность данных!'

    context['user'] = request.user
    return render(request, 'registration.html', context)

def index(request):
    context = dict()
    context['user'] = request.user
    return render(request, 'index.html', context)

def line_to_hw(line, instance_hw):
    if instance_hw is not None:
        line = line[:len(line) - 1].split('!')
        print(line)
        for i in [j for j in instance_hw.punkts.all()]:
            instance_hw.punkts.remove(i)
            instance_hw.save()
        for i in line:
            print(i)
            if len(i) > 0:
                if i[-1] == '>':
                    punkt = Punkt(text= i[:len(i)-1] if len(i) > 1 else '', ready=True)
                else:
                    punkt = Punkt(text=i[:len(i) - 1] if len(i) > 1 else '', ready=False)
            else:
                punkt =Punkt(text = '', ready=False)
            punkt.save()
            instance_hw.punkts.add(punkt)
            instance_hw.save()

@login_required(login_url='/login')
def day_edit(request, day_id):
    context = dict()
    day = Day.objects.filter(id = day_id)[0]
    context['day'] = Day.objects.filter(id = day_id)[0]
    context['day_name'] = Day.objects.filter(id = day_id)[0].name
    context['subs'] = Day.objects.filter(id = day_id)[0].return_schedule()
    context['subjects'] = [i for i in Subject.objects.filter()]
    if request.method == 'POST':
        for i in Subject.objects.filter():
            if 'del_' + str(i.id) in request.POST:
                if i in day.schedule.all():
                    day.schedule.remove(i)
                    day.save()
                    return HttpResponseRedirect('/' + str(day_id))
        form = request.POST
        if 'Change' in form:
            subjs = [i for i in Subject.objects.filter()]
            day = Day.objects.filter(id=day_id)[0]
            for i in [j for j in day.schedule.all()]:
                day.schedule.remove(i)
                day.save()
            for i in subjs:
                res = []
                if str(i.id) in form:
                    day.schedule.add(Subject.objects.filter(name = form[str(i.id)])[0])
                    day.save()


            all_homeworks = [i for i in Hometask.objects.filter()]
            for i in subjs:
                if 'hw_' + str(i.id) in form:
                    print(form['hw_' + str(i.id)])
                    t = i.hometask
                    if t is not None:
                        line_to_hw(form['hw_' + str(i.id)], t)
                    else:
                        temp = Hometask(id = random.randint(100, 9999999))
                        temp.save()
                        i.hometask = temp
                        i.save()
                        t = i.hometask
                        line_to_hw(form['hw_' + str(i.id)], t)

            day.note = form['notaa']
            day.save()
            return HttpResponseRedirect('/' + str(day_id))
        elif 'Add' in form:
            Day.objects.filter(id=day_id)[0].schedule.add(Subject.objects.filter(name = form['additional'])[0])
            Day.objects.filter(id=day_id)[0].save()
            return HttpResponseRedirect('/'+str(day_id))
    return render(request, 'day_edit.html', context)




def login(request):
    context = dict()
    context['user'] = request.user
    if request.method == 'POST':
        form = request.POST
        if not MyUser.objects.filter(username=form['username']):
            context['message'] = 'Нет такого пользователя!'
        else:
            user = authenticate(request, username=form['username'], password=form['password'])
            if user is not None:
                django.contrib.auth.login(request, user)
                context['message'] = 'Вход выполнен!'
                return HttpResponseRedirect('/')
            else:
                context['message'] = 'Не получилось войти:('
    return render(request, 'login.html', context)




def profile(request):
    context = dict()
    context['user'] = request.user
    if request.method == 'POST':
        if 'logout' in request.POST:
            logout(request)
            return redirect('/login')
    return render(request, 'profile.html', context)