from django.urls import path

from . import views
app_name = 'notZ'
urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('registration/', views.registration, name='registration'),
    path('profile/', views.profile, name='profile'),
    path('init/', views.init_days, name='init'),
    path('schedule/', views.show_schedule, name='schedule'),
    path('<int:day_id>/', views.day_edit, name='day_edit'),
]
